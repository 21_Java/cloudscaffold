# -*- coding: utf-8 -*-
from flask import Flask, request, jsonify, Response

import requests

import json

app = Flask(__name__)

@app.route('/auto-merge',methods = ['POST'])
def auto_merge_handler():
    data = request.json
    if not data:
        return jsonify({"status":"error","message":"Empty request"}), 400

    object_kind = data.get('object_kind','Unknow')
    state = data.get('object_attributes',{}).get('state')
    action = data.get('object_attributes',{}).get('action')
    target_branch = data.get('object_attributes',{}).get('target_branch')
    project_id = data.get('project', {}).get('id')
    project_name = data.get('project', {}).get('name', 'Unknown')
    homepage = data.get('project',{}).get('homepage')
    current_mr_iid = data.get('object_attributes',{}).get('iid')
    author_id = data.get('object_attributes',{}).get('author_id')
    author_name = data.get('user',{}).get('name')

    #master -> beta
    if object_kind == 'merge_request' and state == 'merged' and target_branch == 'master':
        return handle_auto_merge(project_id, 'master', ['beta'], project_name, homepage)
    #beta -> alpha
    elif object_kind == 'merge_request' and state == 'merged' and target_branch == 'beta':
        return handle_auto_merge(project_id, 'beta', ['alpha'], project_name, homepage)
    #目标分支为beta/alpha 当达到审核数量后自动合并
    elif object_kind == 'merge_request' and state == 'opened' and action == 'approved' and (target_branch == 'beta' or target_branch == 'alpha'):
        return handle_approve_merge(project_id, str(current_mr_iid), str(author_id), project_name, author_name)
    else:
        return Response(json.dumps({"status": "success", "message": "无需自动合并的请求"}, ensure_ascii=False), content_type='application/json; charset=utf-8')


#自动向下合并
def handle_auto_merge(project_id, source_branch, target_branches, project_name, homepage):
    for target_branch in target_branches:
        #创建合并请求
        creat_mr_result = create_mr(project_id, source_branch, target_branch)
        mr_iid = str(creat_mr_result['iid'])
        #获取请求信息
        mr_detil = query_mr_detail(project_id, mr_iid)
        #是否存在冲突
        has_conflicts = mr_detil["has_conflicts"]
        merge_status = mr_detil["merge_status"]
        web_url = mr_detil["web_url"]
        changes_count = mr_detil["changes_count"]

        #检测时循环调用等待检测完毕
        if merge_status == 'checking':
            condition = True
            while condition:
                condition = query_mr_detail(project_id, mr_iid)["merge_status"] == 'checking'
        #如果可以合并 或者没有冲突
        if merge_status == 'can_be_merged' or not has_conflicts:
            merge_detail = merge_mr(project_id, mr_iid)
            if merge_detail["state"] == 'merged':
                return jsonify({"status": "success", "message": "Auto Merged"}), 200
            else:
                return send_ding_message(project_name, homepage, web_url, source_branch, target_branches, mr_iid)
        else:
            #如果存在变更 但是无法合并 发送钉钉通知
            if changes_count:
                return send_ding_message(project_name, homepage, web_url, source_branch, target_branches, mr_iid)
            else:
                close_mr(project_id, mr_iid)

#beta/alpha 审核数量够以后自动合并
def handle_approve_merge(project_id, current_mr_iid, author_id, project_name, author_name):
    mr_approvals = query_mr_approals(project_id, current_mr_iid)
    approved_bys = mr_approvals["approved_by"]
    approveds = set()
    for approved_by in approved_bys:
        uid = approved_by["user"]["id"]
        #排除自己点击
        if uid != author_id:
            approveds.add(uid)
    approved_len = len(approveds)
    mr_detail = query_mr_detail(project_id, current_mr_iid)
    target_branch = mr_detail["target_branch"]
    source_branch = mr_detail["source_branch"]
    #超过一个时候进入合并逻辑
    if approved_len > 1:
        changes_count = mr_detail["changes_count"]
        web_url = mr_detail["web_url"]
        merge_status = mr_detail["merge_status"]
        state = mr_detail["state"]
        has_conflicts = mr_detail["has_conflicts"]
        if state == "opened":
            #检测中就等待吧~
            if merge_status == "checking":
                boo = True
                while boo:
                    boo = query_mr_detail(project_id, current_mr_iid)["merge_status"] == "checking"
             #如果可以合并 或者没有冲突
            if merge_status == "can_be_merged" or not has_conflicts:
                merge_detail =merge_mr(project_id, current_mr_iid)
                if merge_detail["state"] == 'merged':
                    return jsonify({"status": "success", "message": "Auto Merged"}), 200
                else:
                    send_ding_message(project_name, author_name, web_url, source_branch, target_branch, current_mr_iid)
            else:
                #如果存在变更 但是无法合并 发送钉钉通知
                if changes_count:
                    send_ding_message(project_name, author_name, web_url, source_branch, target_branch, current_mr_iid)
                else:
                    close_mr(project_id, current_mr_iid)
    else:
        print(f"{current_mr_iid} 还不具备可以合并的条件")


#Create MR
def create_mr(project_id, source_branch, target_branch):
    data = {
        "id": project_id,
        "source_branch": source_branch,
        "target_branch": target_branch,
        "title": "Auto Merge"
     }
    response = requests.post(f'https://gitlab.com/api/v4/projects/{project_id}/merge_requests', headers = getRequestHeaders(True), data = json.dumps(data))
    return getReponseResult(response, {})

#Query MR
def query_mr_detail(project_id, mr_iid):
    response = requests.get(f'https://gitlab.com/api/v4/projects/{project_id}/merge_requests/{mr_iid}', headers=getRequestHeaders(False))
    return getReponseResult(response, {})

def query_mr_approals(project_id, mr_iid):
    response = requests.get(f'https://gitlab.com/api/v4/projects/{project_id}/merge_requests/{mr_iid}/approvals', headers=getRequestHeaders(False))
    return getReponseResult(response, {})

#Merge MR
def merge_mr(project_id, mr_iid):
    response = requests.put(f'https://gitlab.com/api/v4/projects/{project_id}/merge_requests/{mr_iid}/merge', headers=getRequestHeaders(True))
    return getReponseResult(response, {})

#Close MR
def close_mr(project_id, mr_iid):
    data = {"state_event": "close"}
    response = requests.post(f'https://gitlab.com/api/v4/projects/{project_id}/merge_requests/{mr_iid}', headers = getRequestHeaders(True), data = json.dumps(data))
    return getReponseResult(response, {})

#封装请求头
def getRequestHeaders(isJson):
    headers = {'PRIVATE-TOKEN':'glpat-Guz9Qmr4ws3zHM7CikPf'}
    if isJson:
        headers.update({'Content-Type': 'application/json'})
    return headers


#解析返回结果
def getReponseResult(response, default_result):
    result = default_result
    try:
        print(response.request.method, response.url, response.status_code, response.text)
        if 200 <= response.status_code < 300:
            result = json.loads(response.text)
    except Exception as err:
        print(err)
    return result

#发送钉钉通知
def send_ding_message(project_name, project_url, web_url, source_branch, target_branch, merge_request_iid):
    # 构建钉钉消息
    text = "#### Auto Merge CI通知\n"
    text = text + "\n"
    text = text + f"- 项目：[{project_name}]({project_url})\n"
    text = text + f"- 合并详情：{source_branch} -> {target_branch}\n"
    text = text + f"- 编号：[#{merge_request_iid}]({web_url})\n"
    text = text + "- 合并状态：**有冲突**\n"
    text = text + "- 合并结果：**无法自动合并**\n"
    text = text + "\n"
    text = text + "> **Auto Merge失败,请相关人员及时处理。**\n"
    # 发送钉钉消息
    req_url = "https://oapi.dingtalk.com/robot/send?access_token=8339e18bb24d563ad8f6d90d78672fd477a54f6df39bdd5d7a2589823f3b6b31"
    markdown = {'title': 'Auto Merge CI通知', 'text': text}
    params = {'msgtype': 'markdown', 'markdown': markdown}
    header_dict = {'Content-Type': 'application/json'}
    requests.post(req_url, data=json.dumps(params), headers=header_dict)

if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0', port=5001)